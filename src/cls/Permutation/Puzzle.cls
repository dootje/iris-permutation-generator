/// Example of using the <class>Iterator</class> class.
Class Permutation.Puzzle [ Abstract, DependsOn = Permutation.Generator ]
{

/// Given a list of N numbers and a list of N-1 arithmetical operators (+, -, *, /),
/// arrange the numbers and operators in such a way as to yield zero.
/// <example>
/// do ##class(Permutation.Puzzle).Solve($lb(6,8,16,25,36),$lb("+","-","*","/"))
/// </example>
/// <pre>
/// 8*25+16/6-36
/// 25*8+16/6-36
/// 25*8+16/36-6
/// 8*25+16/36-6
/// </pre>
/// Other examples:
/// <example>
/// do ##class(Permutation.Puzzle).Solve($lb(1,4,9,12,15),$lb("+","-","*","/"))
/// do ##class(Permutation.Puzzle).Solve($lb(8,11,12,16,18),$lb("+","-","*","/"))
/// do ##class(Permutation.Puzzle).Solve($lb(4,7,12,14,22),$lb("+","-","*","/"))
/// </example>
ClassMethod Solve(pNumbers As %List, pOperators As %List) As %Status
{
	set t1 = $piece($ztimestamp,",",2)
	if '($listvalid(pNumbers)&&$listvalid(pOperators)) quit $$$ERROR($$$GeneralError,"Arguments must be lists")
	if $listlength(pNumbers)'=($listlength(pOperators) + 1) quit $$$ERROR($$$GeneralError,"List lengths must differ by one")
	
	set iteratorNumbers = ##class(Permutation.Generator).%New(pNumbers)
	set iteratorOperators = ##class(Permutation.Generator).%New(pOperators)
	
	while iteratorNumbers.Next()'="" {
		do iteratorOperators.Reset()
		while iteratorOperators.Next()'="" {
			set n = iteratorNumbers.List, o = iteratorOperators.List, e = ""
			for k=1:1:$listlength(o) set e = e_$listget(n,k)_$listget(o,k)
			set e = e_$listget(n,k+1)_"=0"
			set e = "if "_e_" write """_e_""",!"
			xecute e
		}
	}
	write "Execution took "_($piece($ztimestamp,",",2)-t1)_" s",!
	return $$$OK
}

}
