/// Iterator for generating permutations.
/// Based on the <a href="https://www.quickperm.org/">QuickPerm Countdown algorithm</a>.
Class Permutation.Generator Extends %RegisteredObject
{

/// Collection of elements to permutate.
Property List As %List [ MultiDimensional, ReadOnly ];

/// The number of elements in the list.
Property Size As %Integer [ ReadOnly ];

Property p As %Integer [ Internal, MultiDimensional ];

Property i As %Integer [ InitialExpression = 0, Internal ];

Property j As %Integer [ Internal ];

/// Constructor. Expects a list constructed with <code>$LISTBUILD</code>.
Method %OnNew(initvalue As %List = "") As %Status
{
	if initvalue="" quit $$$ERROR($$$GeneralError,"Initial value is required")
	if '$listvalid(initvalue) quit $$$ERROR($$$GeneralError,"Initial value must be a list")
	
	set i%Size = $listlength(initvalue)
	for k=0:1:i%Size-1 set i%List(k) = $listget(initvalue, k+1), i%p(k) = k
	set i%p(i%Size) = i%Size
	quit $$$OK
}

/// Getter for the List property. Converts the subscripted array to a $LIST.
Method ListGet() As %String [ Internal ]
{
	set result = ""
	for k=0:1:i%Size-1 set result = result _ $listbuild(i%List(k))
	quit result
}

/// Returns the next permutation.
/// If all permutations are exhausted the return value is an empty string.
Method Next() As %List
{
	if i%i=0 {
		set i%i = 1
		return ..List
	} elseif i%i=i%Size {
		return ""
	} else {
		set i%p(i%i) = i%p(i%i) - 1
		if i%i#2 {
			set i%j = i%p(i%i)
		} else {
			set i%j = 0
		}
		set $listbuild(i%List(i%i),i%List(i%j)) = $listbuild(i%List(i%j),i%List(i%i)), i%i = 1
		while i%p(i%i)=0 {
			set i%p(i%i) = i%i, i%i = i%i + 1
		}
		return ..List
	}
}

/// Re-initializes the calculation.
/// Note that the order of the elements is not defined.
Method Reset() As %Status
{
	for k=0:1:i%Size-1 set i%p(k) = k
	set i%p(i%Size) = i%Size, i%i = 0
	quit $$$OK
}

}
