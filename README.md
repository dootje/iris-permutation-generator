# iris-permutation-generator

Example of an ObjectScript class for generating permutations.
It implements the [QuickPerm Countdown algorithm](https://www.quickperm.org/).

## Usage

Import `Permutation.Generator.cls` into any IRIS namespace.

The constructor of this class accepts a %List argument.

## Sample code

In a terminal, try this to see it in action:

```objectscript
set iter = ##class(Permutation.Generator).%New($lb(1,2,3))
while iter.Next()'="" { write $listtostring(iter.List),! }
1,2,3
2,1,3
3,1,2
1,3,2
2,3,1
3,2,1
```

The `Permutation.Puzzle` class contains an example that leverages the generator class.
